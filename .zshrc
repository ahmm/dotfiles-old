# If you come from bash you might have to change your $PATH.
export PATH=$HOME/.scripts:$PATH

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="robbyrussell"

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS=true

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

plugins=(git vi-mode zsh-nvm fzf)

source $ZSH/oh-my-zsh.sh

####################
# User configuration
####################
HISTSIZE=5000 
HISTFILESIZE=10000
setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_SPACE
setopt HIST_FIND_NO_DUPS

export EDITOR=nvim

# Compilation flags
# export ARCHFLAGS="-arch x86_64"


bindkey -M menuselect '^h' vi-backward-char
bindkey -M menuselect '^k' vi-up-line-or-history
bindkey -M menuselect '^l' vi-forward-char
bindkey -M menuselect '^j' vi-down-line-or-history

###################
# Fuzzy find
###################
export FZF_DEFAULT_OPTS="--height 75% --color=bg+:#282828,gutter:#282828"
export FZF_DEFAULT_COMMAND='fd --hidden --follow'
export FZF_ALT_C_COMMAND='fd --hidden --follow --type d -E "site-packages" -E "node_modules" -E ".git" . $HOME'

fzf_cd_into_file_dir () {
  local file
  local dir
  file=$(fd --type f | fzf) 
  dir=$(dirname "$file")
  cd "$dir"
}

fzf_edit_file () {
    local files=$(
        fd --type f | fzf --multi -0 -1
    )
    if [[ -n $files ]]; then
        $EDITOR $files
    fi
}

# Checkout git branch (including remote branches) sorted by most recent commit
fgc() {
  local branches branch
  branches=$(git branch --all --sort=committerdate | grep -v HEAD) &&
  branch=$(echo "$branches" |
           fzf-tmux -d $(( 2 + $(wc -l <<< "$branches") )) +m) &&
  git checkout $(echo "$branch" | sed "s/.* //" | sed "s#remotes/[^/]*/##")
}

# fzf_docker() {
# }

alias fe=fzf_edit_file
alias fcd=fzf_cd_into_file_dir
alias b='buku --suggest'

######################
# Functions
#####################
# Make a directory and cd into it
mcd() {
    mkdir -p $1
    cd $1
}

# Cd and ls contents
cd() {
    builtin cd "$@" && ls
}

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"
