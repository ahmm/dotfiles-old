# Copyright (c) 2011, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import subprocess
from typing import List  # noqa: F401

from libqtile.config import Key, Screen, Group, Drag, Click
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook


mod = "mod4"
home = os.path.expanduser('~')
terminal = 'kitty'

keys = [
    Key([mod], "h", lazy.layout.left()),
    Key([mod], "j", lazy.layout.down()),
    Key([mod], "k", lazy.layout.up()),
    Key([mod], "l", lazy.layout.right()),

    Key([mod, "control"], "h", lazy.layout.shuffle_left()),
    Key([mod, "control"], "j", lazy.layout.shuffle_down()),
    Key([mod, "control"], "k", lazy.layout.shuffle_up()),
    Key([mod, "control"], "l", lazy.layout.shuffle_l()),

    Key([mod], "f", lazy.window.toggle_floating()), 
    Key([mod, "shift"], "f", lazy.window.toggle_fullscreen()), 

    # Switch window focus to other pane(s) of stack
    Key([mod], "Tab", lazy.layout.next()),

    Key([mod], "Return", lazy.spawn(terminal)),
    Key([mod, "shift"], "Return", lazy.spawn(f'{terminal} tmux')),

    # Toggle between different layouts as defined below
    Key([mod], "space", lazy.next_layout()),
    Key([mod, "shift"], "q", lazy.window.kill()),

    Key([mod, "control"], "r", lazy.restart()),
    Key([mod, "control"], "q", lazy.shutdown()),
    #Key([mod], "r", lazy.spawncmd()),

    Key([mod], 'n', lazy.next_screen()),
    Key([mod], "slash", lazy.findwindow()),

    Key([mod], 'd', lazy.spawn('rofi -show run')),
    Key([mod], 'r', lazy.spawn('rofi -show drun -show-icons')),

    # Take screeshots:
    Key([], 'Print', lazy.spawn(f'/usr/bin/scrot --select /home/ahmm/Pictures/Screenshots/screenshot_%Y_%m_%d_%H_%M_%S.png')),

    # Music keys:
    Key([], "XF86AudioRaiseVolume",
        lazy.spawn("amixer sset Master 5%+")),
    Key([], "XF86AudioLowerVolume",
        lazy.spawn("amixer sset Master 5%-")),
    Key([], "XF86AudioMute",
        lazy.spawn("amixer sset Master toggle")),
]


group_names = [
    ('1', '1: '),
    ('2', '2: '),
    ('3', '3: '),
    ('4', '4'),
    ('6', '6'),
    ('7', '7'),
    ('8', '8: '),
    ('9', '9: '),
    ('0', '0: '),
]

groups = [
    Group(name=name, label=label) for (name, label) in group_names
]

for i in groups:
    keys.extend([
        # mod1 + letter of group = switch to group
        Key([mod], i.name, lazy.group[i.name].toscreen()),

        # mod1 + shift + letter of group = switch to & move focused window to group
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name)),
    ])


color_focus = '#215578'
color_normal = '#000000'

layouts = [
    #layout.Stack(num_stacks=2)
    layout.MonadTall(
        ratio=0.60,
        border_focus=color_focus,
        border_normal=color_normal
    ),
    layout.Max(),
    layout.Bsp(
        border_focus=color_focus,
        border_normal=color_normal
    )
]

widget_defaults = dict(
    font='sans',
    fontsize=12,
    padding=2,
)
extension_defaults = widget_defaults.copy()


screens = [
    Screen(top=bar.Bar(
        [
            widget.CurrentLayoutIcon(
                scale=0.65,
                padding=5
            ),
            widget.GroupBox(
                hide_unused=True,
                borderwidth=2,
                highlight_method='block',
                inactive="FFFFFF",
            ),
            widget.sep.Sep(
                padding=10,
                linewidth=0
            ),
            widget.WindowName(),
            widget.Systray(),
            widget.Clock(
                format="%H:%M",
                padding=8
            ),
        ],
        24
    )),
    Screen(top=bar.Bar(
        [
            widget.CurrentLayoutIcon(scale=0.65),
            widget.GroupBox(
                hide_unused=True,
                borderwidth=2,
                highlight_method='block',
                inactive="FFFFFF",
            ),
            widget.sep.Sep(
                padding=5,
                linewidth=0
            ),
            widget.WindowName(),
            widget.Clock(),
        ],
        24
    )),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = True
floating_layout = layout.Floating(float_rules=[
    {'wmclass': 'confirm'},
    {'wmclass': 'dialog'},
    {'wmclass': 'download'},
    {'wmclass': 'error'},
    {'wmclass': 'file_progress'},
    {'wmclass': 'notification'},
    {'wmclass': 'splash'},
    {'wmclass': 'toolbar'},
    {'wmclass': 'confirmreset'},  # gitk
    {'wmclass': 'makebranch'},  # gitk
    {'wmclass': 'maketag'},  # gitk
    {'wname': 'branchdialog'},  # gitk
    {'wname': 'pinentry'},  # GPG key password entry
    {'wmclass': 'ssh-askpass'},  # ssh-askpass
])
auto_fullscreen = True
focus_on_window_activation = "smart"

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"


def configure_monitors():
    wallpaper = '/home/ahmm/Pictures/Wallpapers/wallpaper.png'

    subprocess.run('xrandr --output VGA-0 --mode 1680x1050 --pos 0x0 --rotate normal --output LVDS-0 --primary --mode 1600x900 --pos 0x1050 --rotate normal'.split())
    subprocess.run(f'feh --bg-scale {wallpaper}'.split())


configure_monitors()


@hook.subscribe.startup_once
def autostart():
    subprocess.run([f'{home}/.config/qtile/autostart.sh'])


# Perequsites:
# rofi
# otf-font-awesome-5
# network manager (for nm-applet)
# feh
# pa-applet
