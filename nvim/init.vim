" install vimplug first - https://github.com/junegunn/vim-plug

""""""""""""""""""""""""""
" Plugins
""""""""""""""""""""""""""
call plug#begin('~/.nvim/plugged')

" Fzf for fuzzy searching:
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'

" Lightline
Plug 'itchyny/lightline.vim'

" Vim wiki
Plug 'vimwiki/vimwiki', {'branch': 'dev'}

" Colorschemes
Plug 'morhetz/gruvbox' 

" Collection of language packs
Plug 'sheerun/vim-polyglot'
      
" Auto closing parantheses etc.
Plug 'jiangmiao/auto-pairs'

" Tmux integration for moving simlessly between vim splits and tmux panes:
Plug 'christoomey/vim-tmux-navigator'

" Git integration
" Plug 'tpope/vim-fugitive'
"
" Show git diff in the gutter:
" Plug 'airblade/vim-gitgutter'

" LSP Integration
Plug 'neoclide/coc.nvim', {'branch': 'release'}

" Emmet
Plug 'mattn/emmet-vim'

" Css colors
Plug 'ap/vim-css-color'


call plug#end()

""""""""""""""""""""""""""
" General config
""""""""""""""""""""""""""
" Set to auto read when a file is changed from the outside
set autoread

" Line numbers, syntax highlighting etc.
set number
set relativenumber
set cursorline
syntax enable

" Searching
set ignorecase
set smartcase
set hlsearch

" Yank to system clipboard
set clipboard=unnamedplus

" Backups - turn them off, since what's important will be in git anyway
set nobackup
set nowb
set noswapfile

" Tabs, spaces
set expandtab
set smarttab
set shiftwidth=4
set tabstop=4
set ai "auto indent
set si "smart indent

" List invisible characters like tabs and trailing whitespace
set list
set showbreak=↪
set listchars=tab:»\ ,extends:›,precedes:‹,nbsp:·,trail:·

" Fold code by indent, start with all folds open
set foldmethod=indent
set foldlevelstart=99

""""""""""""""""""""""""""
" Key bindings
""""""""""""""""""""""""""
let mapleader = " "        "set leader to space
let maplocalleader = '\'  "set localleader to backslash

" Move lines up/down easily
" nnoremap <leader>j :m+<cr>==
" nnoremap <leader>k :m-2<cr>==
" xnoremap <leader>j :m'>+<cr>gv=gv
" xnoremap <leader>k :m-2<cr>gv=gv

" Sorting
" xnoremap <leader>ss :sort i<cr>
" xnoremap <leader>sS :sort! i<cr>
" xnoremap <leader>su :sort u<cr>

" Uppercase the word before the cursor in insert mode
inoremap <C-u> <esc>vbUea

" Change directory to directory of the current file (and print it):
nnoremap <leader>cd :cd %:p:h<cr>:pwd<cr>

""""""""""""""""""""""
" Windows, tabs, buffers
""""""""""""""""""""""""""
" Open splits to the right/bottom which is more intuitive
set splitbelow splitright

" Smart way to move between splits
nnoremap <C-h> <C-W>h
nnoremap <C-j> <C-W>j
nnoremap <C-k> <C-W>k
nnoremap <C-l> <C-W>l

" Resize splits by 5/10 instead of 1
nnoremap <silent> <Leader>- :resize -5<cr>
nnoremap <silent> <Leader>+ :resize +5<cr>
nnoremap <silent> <Leader>> :vertical resize +10<cr>
nnoremap <silent> <Leader>< :vertical resize -10<cr>

" close other buffers
" nnoremap <leader>bm :%bd<bar>e#<cr>      
" nnoremap <leader>bM :%bd!<bar>e#<cr>      

" close this buffer without closing the split
" nnoremap <leader>bd :bn<bar>bd#<cr> 
" nnoremap <leader>bD :bn!<bar>bd#<cr> 

" Moving between buffers  - fuzzy and search is better
" nnoremap <silent> <leader>bp :bprevious<cr>
" nnoremap <silent> <leader>bn :bnext<cr>
" nnoremap <silent> <leader>b0 :bfirst<cr>
" nnoremap <silent> <leader>b$ :blast<cr>

" Show a list of buffers when you type :b
set wildmenu

""""""""""""""""""""""""""
" Moving, searching
""""""""""""""""""""""""""
" Searching
set ignorecase
set smartcase
set hlsearch

" Keep cursor position when searching with '*'
nnoremap * *<c-o>

" Remove search highlight
nnoremap <leader><cr> :nohlsearch<cr>

"""""""""""""""""""""""""""
" Plugin settings 
"""""""""""""""""""""""""""
" FZF
nnoremap <leader><Space> :Buffers<CR>

nnoremap <leader>ff :Files<CR>
nnoremap <leader>fg :GFiles<CR>
nnoremap <leader>fr :Rg<CR>


"""""""""""""""""""""""""""
" Vimwiki
"""""""""""""""""""""""""""
let old_wiki = {}

if has('win32')
    let old_wiki.path = 'C:\Users\tomek\Nextcloud\wiki\'
else
    let old_wiki.path = '~/Nextcloud/wiki/'
endif

let everyday_wiki = {}
let everyday_wiki.syntax = 'markdown'
let everyday_wiki.ext = '.md'
if has('win32')
    let everyday_wiki.path = 'C:\Users\tomek\Nextcloud\Notes\'
else
    let everyday_wiki.path = '~/Nextcloud/Notes/everyday'
endif


let g:vimwiki_list = [everyday_wiki, 
                      \ {'syntax': 'markdown', 'ext': '.md', 'path': '~/Nextcloud/Notes/thoughts'},
                      \ {'syntax': 'markdown', 'ext': '.md', 'path': '~/Nextcloud/Notes/coding'},
                      \ old_wiki]

" Use pretty alternating colors for headers
let g:vimwiki_hl_headers = 1

" Remap + so that it doesn't iterfere with builtin +
:nmap ++ <Plug>VimwikiNormalizeLink
:vmap ++ <Plug>VimwikiNormalizeLinkVisual

" Custom function for folding - because folding by expr doesn't handle
" code blocks well:
let g:vimwiki_folding = 'custom'

function! VimwikiFoldLevelCustom(lnum)
  let pounds = strlen(matchstr(getline(a:lnum), '^#\+'))
  if (pounds)
    return '>' . pounds  " start a fold level
  endif
  return '=' " return previous fold level
endfunction

augroup VimrcAuGroup
  autocmd!
  autocmd FileType vimwiki setlocal foldmethod=expr |
    \ setlocal foldenable | set foldexpr=VimwikiFoldLevelCustom(v:lnum)
augroup END


"""""""""""""""""""""""""""
" Appearance
"""""""""""""""""""""""""""
set termguicolors

colorscheme gruvbox
set background=dark

" enable italic text
let g:gruvbox_italic = '1'

let g:lightline = {
  \ 'colorscheme': 'gruvbox',
  \ }

" We don't need current mode on the bottom because it's handled by the lightline:
set noshowmode 

"""""""""""""""""""""""""""
" Abbreviations
"""""""""""""""""""""""""""
" Type :vh to open help in vertical window
cnoreabbrev vh vert help

" iabbrev @@ ahmmz@protonmail.com

" frequent mistakes:
" iabbrev reponse response

"""""""""""""""""""""""""""
" Language-specific
"""""""""""""""""""""""""""
let g:python_host_prog = '/usr/bin/python2'
let g:python3_host_prog = '/usr/bin/python3'

augroup python
"     autocmd!
"     autocmd FileType python nnoremap <buffer> <localleader>c I# <esc>
"     autocmd FileType python nnoremap <buffer> <localleader>cc ^dW<esc>
"     autocmd FileType python nnoremap <localleader>i :!isort %<cr><cr>

    " This is so that comments are folded correctly:
    autocmd FileType python set foldignore=
augroup END

augroup filetypes
    autocmd FileType vim nnoremap <buffer> <localleader>c I" <esc>
    autocmd FileType vim nnoremap <buffer> <localleader>cc ^dW<esc>

    " 2 spaces for web
    autocmd FileType html set tabstop=2 shiftwidth=2
    autocmd FileType htmldjango set tabstop=2 shiftwidth=2

    autocmd FileType javascript set tabstop=2 shiftwidth=2
    autocmd FileType json set tabstop=2 shiftwidth=2

    autocmd FileType css set tabstop=2 shiftwidth=2 iskeyword+=-
    autocmd FileType scss set tabstop=2 shiftwidth=2 iskeyword+=-
    autocmd FileType vue set tabstop=2 shiftwidth=2


    " 2 spaces for vimwiki 
    autocmd FileType vimwiki set tabstop=2 shiftwidth=2 textwidth=90
    autocmd BufRead,BufNewFile *.md setlocal textwidth=90

augroup END

let g:polyglot_disabled = ['coffee-script']

"""""""""""""""""""""""""""
" Snippets
"""""""""""""""""""""""""""
" Insert breakpoint below line in python
" nnoremap ,pudb oimport pudb; pudb.set_trace()<esc>
" nnoremap ,pdb oimport pdb; pdb.set_trace()<esc>
nnoremap ,ipdb import ipdb; ipdb.set_trace(context=10)<esc>

" django urls:
" docstring
" new class (with __init__ method)
" exceptions try trye try/except/else tryf try/except/finally tryef
" try/except/else/finally
" vue component
" vue store

"""""""""""""""""""""""""""
" Misc
"""""""""""""""""""""""""""
augroup Vimrc
    autocmd!

    " Update splits when the window is resized
    autocmd VimResized * :wincmd =

    " Only show cursor line in current window and insert mode
    autocmd WinLeave,InsertEnter * set nocursorline
    autocmd WinEnter,InsertLeave * set cursorline
augroup END

let g:vim_jsx_pretty_colorful_config = 1


"""""""""""""""""""""""""""
" coc
"""""""""""""""""""""""""""
" Use tab for trigger completion with characters ahead and navigate.
" Use command ':verbose imap <tab>' to make sure tab is not mapped by other plugin.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()

" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current position.
" Coc only does snippet and additional edit on confirm.
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"

" Use `[g` and `]g` to navigate diagnostics
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" Remap keys for gotos
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}
