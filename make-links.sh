#!/bin/bash

HOME_DIRECTORY="/home/ahmm"
SCRIPT_DIRECTORY=$(dirname `realpath $0`)

I3_CONFIG_SOURCE="$SCRIPT_DIRECTORY/i3/config"
I3_CONFIG_DESTINATION="$HOME_DIRECTORY/.config/i3/config"

I3_BLOCKS_SOURCE="$SCRIPT_DIRECTORY/i3/i3blocks"
I3_BLOCKS_DESTINATION="$HOME_DIRECTORY/.config/i3blocks"

BASHRC_SOURCE="$SCRIPT_DIRECTORY/.bashrc"
BASHRC_DESTINATION="$HOME_DIRECTORY/.bashrc"

NVIM_SOURCE="$SCRIPT_DIRECTORY/nvim/init.vim"
NVIM_DESTINATION="$HOME_DIRECTORY/.config/nvim/init.vim"

RANGER_SOURCE="$SCRIPT_DIRECTORY/ranger/"
RANGER_DESTINATION="$HOME_DIRECTORY/.config/ranger"

declare -A destinations=( 
    [$I3_CONFIG_SOURCE]=$I3_CONFIG_DESTINATION 
    [$BASHRC_SOURCE]=$BASHRC_DESTINATION 
    [$NVIM_SOURCE]=$NVIM_DESTINATION 
    [$RANGER_SOURCE]=$RANGER_DESTINATION 
    [$I3_BLOCKS_SOURCE]=$I3_BLOCKS_DESTINATION
)

for file in "${!destinations[@]}"; do
    # create directory if it doesn't exist:
    dir=$(dirname "${destinations[$file]}")
    mkdir -p $dir

    ln -s $file ${destinations[$file]}
done

