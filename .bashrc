# If not running interactively, don't do anything
[[ $- != *i* ]] && return
# try to attach to last session if not create new one
#test -z "$TMUX" && (tmux attach || tmux new-session)

# Activate vim mode with <escape>
set -o vi

shopt -s checkwinsize  # check the window size after each command

# bash history
HISTCONTROL=ignoreboth  # ignore duplicates & commands starting with whitespace
HISTSIZE=5000 
HISTFILESIZE=10000
shopt -s histappend  # append to the history file, don't overwrite it

###################
# Alias definitions
###################
shopt -s expand_aliases

# Colored output for ls & grep:
alias ls='ls --color=auto'
alias grep='grep --color=auto'

# Ask for confirmation when you want to rewrite a file:
alias mv='mv -i'
alias cp='cp -i'
alias ln='ln -i'

alias rm='rm -I --preserve-root'
alias chown='chown --preserve-root'
alias chmod='chmod --preserve-root'

export EDITOR=nvim

# Command prompt
export PS1="\[\e[0;32m\]\u@\h \w$ \[\e[0m\]"

# Bash completion
[ -r /usr/share/bash-completion/bash_completion ] && . /usr/share/bash-completion/bash_completion
complete -cf sudo

######################
# Fuzzy finding
#####################
[ -f /usr/share/fzf/key-bindings.bash ] && source /usr/share/fzf/key-bindings.bash
[ -f /usr/share/fzf/completion.bash ] && source /usr/share/fzf/completion.bash

# Search in dotdirectories
export FZF_DEFAULT_COMMAND='fd --type f'
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
export FZF_ALT_C_COMMAND="fd --type d"
export FZF_DEFAULT_OPTS="--height 75% --color=bg+:#282828,gutter:#282828"

# __fzf_cd__() {
#    local file
#    local dir
#    file=$(fzf +m -q "$1") && dir=$(dirname "$file") && printf "cd \"$dir\""
# }

fzf_cd_into_file_dir () {
  local file
  local dir
  file=$(fd --type f | fzf) 
  dir=$(dirname "$file")
  cd "$dir"
}

fzf_edit_file () {
    local files=$(
        fd --type f | fzf --multi -0 -1
    )
    if [[ -n $files ]]; then
        $EDITOR $files
    fi
}

# fzf_docker() {
# }

alias fe=fzf_edit_file
alias fcd=fzf_cd_into_file_dir

######################
# Functions
#####################
# Make a directory and cd into it
mcd() {
    mkdir -p $1
    cd $1
}

# Cd and ls contents
cd() {
    builtin cd "$@" && ls
}



######################
# Auto-added
#####################
[ -f ~/.fzf.bash ] && source ~/.fzf.bash

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
